import { Component } from "react";
import TitleImage from "./image/TitleImage";
import TitleText from "./text/TitleText";

class TitleComponent extends Component {
    render() {
        return (
            <>
                <TitleText />
                <TitleImage />
            </>
        )
    }
}

export default TitleComponent;